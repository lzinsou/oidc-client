import pytest

from oidc_client.pkce import PKCESecret


@pytest.mark.parametrize("length", [32, 64, 128])
def test_pkce_secret(length):
    pkce_secret = PKCESecret()

    assert isinstance(pkce_secret.value, str)
    assert str(pkce_secret) == pkce_secret.value
    assert bytes(pkce_secret) == pkce_secret.value.encode()


def test_pkce_secret_challenge():
    pkce_secret = PKCESecret()

    assert pkce_secret.challenge_method == "S256"

    pkce_secret.value = "ks02i3jdikdo2k0dkfodf3m39rjfjsdk0wk349rj3jrhf"
    assert isinstance(pkce_secret.challenge, bytes)
    assert pkce_secret.challenge == b"2i0WFA-0AerkjQm4X4oDEhqA17QIAKNjXpagHBXmO_U"
